
#Steps

##Install/Upgrade to latest Nodejs 

you can manage node using NVM - [How To NVM?](https://www.digitalocean.com/community/tutorials/how-to-install-node-js-with-nvm-node-version-manager-on-a-vps).

```
➜  client git:(master) ✗ nvm list
         v5.3.0
         v7.2.1
->       v7.6.0
         system
default -> node (-> v7.6.0)
node -> stable (-> v7.6.0) (default)
stable -> 7.6 (-> v7.6.0) (default)
iojs -> N/A (default)
➜  client git:(master) ✗ node -v
v7.6.0

```

## Install/Upgrade NPM
Install/upgrade to NPM 5 and install node modules in both client/server projects.
```
npm install npm@latest -g
cd optimus-prime/server
npm install
cd optims-prime/client
npm install
```

## Manage Database
Assuming postgres is installed and postico or similar client to access psql.
```
cd ./server/db
sh reset_db.sh
```

## Environment File
```
cd ./server
touch .env
add below key value pair to .env file
APP_SECRET="$@!Ram"
```

## Run Server
```
cd ./server
npm run server
```
## Run client to watch for any changes.Run it another browser
```
cd ./client 
npm run build:watch
```

## Format JS files in both server/client
```
cd ./(client/Server)
npm run js:preetify
```

preetier is used for formatting any js files

## Format Marko templates( HTML formatter)
cd ./client
npm run marko:preetify

## Login to website - 
[Login](http://localhost:3000)
username: admin password:sairam

### Technology stack
Express
Express-JWT
GraphQL
Express GraphQL
Objection.js for ORM
Knex for migration
Babel for ES6 support
webpack for bundling
Markojs for SPA
Bulma.io for css framework
Page.js for SPA routing.
