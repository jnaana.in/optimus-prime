var path = require('path');
var destFile = path.join("..", "server", "public", "js", "bundle.js");
var BabelPlugin = require("babel-webpack-plugin");


module.exports = {
	entry: "./app/js/main.js",
	output: {
		path: __dirname,
		filename: destFile
	},
	resolve: {
		extensions: ['.js', '.marko']
	},
	module: {
		rules: [
			{
				test: /\.js$/,
				exclude: /(node_modules|bower_components)/,
				use: {
					loader: 'babel-loader',
					options: {
						presets: ['env'],
						plugins: ['add-module-exports']

					}
				}
			},
			{
				test: /\.marko$/,
				loader: 'marko-loader'
			}
		]

	}
};