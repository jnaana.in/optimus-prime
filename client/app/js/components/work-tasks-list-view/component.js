import timeago from "timeago.js";
import gpl from "graphql-tag";
import { client } from "../../services/graphQLServiceApi";
import WorkTask from "../../models/WorkTask";
import { handleGraphQlErrors } from "../../utils/errors";
import { showInfoMessage } from "../../utils/toaster";

export default {
  getAllTasks() {
    var self = this;
    self.setLoadingTask(true);
    client
      .query({
        query: gpl`
               query getWorkTasks($page:Int!,$limit:Int!,$orderBy:SortOrder,$filters:WorkTaskFilters){
                 workTasks(page:$page,limit:$limit,orderBy:$orderBy,filters:$filters){
                   nodes{
                     taskId
                     title
                     description
                     requestType {
                       id
                       table
                       field
                       displayValue
                       displayLabel
                     }
                     status {
                       id
                       table
                       field
                       displayValue
                       displayLabel
                     }
                     assignmentGroup{
                       displayValue
                       displayLabel
                     }
                     assignee {
                       userName
                       email
                     }
                     caller {
                       userName
                       email
                     }
                     createdAt
                     updatedAt
                   }
                   pageInfo{
                     total
                     totalPages
                     hasNext
                   }
                 }
               }
          `,
        variables: {
          page: this.state.currentPage,
          limit: 10,
          orderBy: {
            field: self.pluck(self.state.selectedSortField, "displayValue"),
            direction: self.state.isOrderbyAscending ? "asc" : "desc"
          },
          filters: {
            requestType: self.pluck(
              this.state.selectedFilterTypes,
              "displayValue"
            ),
            status: self.pluck(this.state.selectedFilterStatus, "displayValue"),
            caller: self.pluck(this.state.selectedCallers, "userName")
          }
        },
        fetchPolicy: "network-only"
      })
      .then(promise => {
        self.setTasks(promise.data.workTasks.nodes);
        self.state.shouldFetchNextPage =
          promise.data.workTasks.pageInfo.hasNext;
      })
      .then(promise => {
        setTimeout(function() {
          self.setLoadingTask(false);
        }, 200);
      })
      .catch(handleGraphQlErrors);
  },
  onCreate(input) {
    this.state = {
      tasks: {},
      isLoadingTasks: input.isLoadingTasks || false,
      selectedSortField: input.selectedSortField || [],
      isOrderbyAscending: input.isOrderbyAscending || false,
      selectedFilterTypes: input.selectedFilterTypes || [],
      selectedFilterStatus: input.selectedFilterStatus || [],
      selectedCallers: input.selectedCallers || [],
      currentPage: 0,
      shouldFetchNextPage: true,
      showTask: false,
      currentTask: {}
    };

    this.timeago = timeago;
  },
  onInput(input) {
    if (input.selectedSortField) {
      this.state.selectedSortField = input.selectedSortField;
      this.state.tasks = [];
      this.state.shouldFetchNextPage = true;
      this.state.currentPage = 0;
    }

    if (typeof input.isOrderbyAscending !== "undefined") {
      this.state.isOrderbyAscending = input.isOrderbyAscending;
      this.initState();
    }

    if (this.state.selectedFilterTypes !== input.selectedFilterTypes) {
      this.initState();
    }

    if (this.state.selectedCallers !== input.selectedCallers) {
      this.initState();
    }

    if (this.state.selectedFilterStatus !== input.selectedFilterStatus) {
      this.initState();
    }

    this.state.selectedFilterTypes = input.selectedFilterTypes;
    this.state.selectedFilterStatus = input.selectedFilterStatus;
    this.state.selectedCallers = input.selectedCallers;
    this.getAllTasks();
  },
  onMount() {
    let self = this;
    this.subscribeTo(this.el).on("scroll", () => {
      if (self.el.scrollTop + self.el.offsetHeight + 5 > self.el.scrollHeight) {
        self.callNextPage();
      }
    });
  },
  setLoadingTask(isLoading) {
    this.state.isLoadingTasks = isLoading;
  },
  closeShowTask(task) {
    this.setState("showTask", false);
    this.setState("currentTask", {});
  },
  deleteTask(task) {
    var self = this;
    client
      .mutate({
        mutation: gpl`
                   mutation DeleteWorkTask($taskId:String!)
                    {
                     deleteWorkTask(taskId:$taskId)
                   }
                  `,
        variables: {
          taskId: task.taskId
        }
      })
      .then(promise => {
        if (promise.data.deleteWorkTask === "ok") {
          self.state.tasks = this.state.tasks.filter(thisTask => {
            return thisTask.taskId !== task.taskId;
          });
          if (
            Object.keys(self.state.currentTask).length > 0 &&
            self.state.currentTask.taskId === task.taskId
          ) {
            self.state.currentTask = {};
            self.state.showTask = false;
          }

          showInfoMessage("Sucessfully deleted the task");
        }
      })
      .catch(handleGraphQlErrors);
  },
  callNextPage() {
    var self = this;
    this.state.currentPage = this.state.currentPage + 1;
    if (this.state.shouldFetchNextPage) {
      self.getAllTasks();
    }
  },
  onMouseOverTaskItem(task, evt) {
    this.updateTaskState(task, "isCurrent", true);
    this.forceUpdate();
  },
  onMouseOutTaskItem(task, evt) {
    this.updateTaskState(task, "isCurrent", false);
    this.forceUpdate();
  },
  updateTaskState(task, property, value) {
    for (var i = 0; i < this.state.tasks.length; i++) {
      if (this.state.tasks[i].taskId === task.taskId) {
        this.state.tasks[i][property] = value;
      }
    }
  },
  setTasks(tasks) {
    var self = this;
    const data = tasks.map(task => {
      return new WorkTask(
        task.taskId,
        task.title,
        task.description,
        task.requestType,
        task.status,
        task.caller,
        task.assignee,
        task.createdAt,
        task.updatedAt
      );
    });
    self.state.tasks = self.state.tasks.concat(data);
  },
  initState() {
    this.state.tasks = [];
    this.state.currentPage = 0;
    this.state.shouldFetchNextPage = true;
  },
  pluck(arr, key) {
    return arr.map(item => {
      return item[key];
    });
  }
};
