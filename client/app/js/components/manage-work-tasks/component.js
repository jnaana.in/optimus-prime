import appContext from "../../services/globalAppService";
import WorkTask from "../../models/WorkTask";
import gpl from "graphql-tag";
import { client } from "../../services/graphQLServiceApi";
import { handleGraphQlErrors } from "../../utils/errors";
import { first } from "../../utils/helpers";
import customSelect from "../custom-select";

export default {
  onCreate(input) {
    this.state = {
      currentTask: null,
      isLoading: false,
      taskId: "",
      workTaskFields: {
        assigneeQuery: this.getAssigneeQuery(),
        data: {}
      }
    };
    var router = appContext.get("router");
    this.getWorkTaskFields();
    this.getTask(input.params.params.id);
    this.state.taskId = input.params.params.id;
  },
  onInput(input) {},
  onMount() {
    setTimeout(() => {
      this.setupAssigneeComponent();
    }, 1000);
  },
  getTask(taskId) {
    var self = this;
    self.setLoadingTask(true);
    return client
      .query({
        query: gpl`
              query GetTask($taskId:String!){

                workTask(taskId:$taskId){
                  taskId
                  title
                  description
                  requestType{
                    id
                    displayLabel
                    displayValue
                  }
                  status{
                    id
                    displayLabel
                    displayValue
                  }
                  assignmentGroup{
                    displayLabel
                    displayValue
                  }
                  assignee{
                    userName
                    email
                  }
                  caller{
                    userName
                    email
                  }
                  createdAt
                  updatedAt
                }

              }

          `,
        variables: {
          taskId: taskId
        },
        fetchPolicy: "network-only"
      })
      .then(promise => {
        self.setWorkTask(promise.data.workTask);
        return promise;
      })
      .then(promise => {
        setTimeout(function() {
          self.setLoadingTask(false);
        }, 400);
      })
      .catch(handleGraphQlErrors);
  },
  getWorkTaskFields() {
    var self = this;
    client
      .query({
        query: gpl`
              query GetFilter {

                status: choiceListByTableAndField(table: "work_task", field: "status") {
                  ...ChoiceListInfo
                }

                requestType:choiceListByTableAndField(table:"work_task",field:"request_type"){
                  ...ChoiceListInfo
                }

                assignmentGroups:assignmentGroups {
                  displayLabel
                  displayValue
                }

              }

              fragment ChoiceListInfo on ChoiceList{
                displayLabel
                displayValue
                id
              }

          `
      })
      .then(promise => {
        self.state.workTaskFields.data = promise.data;
      })
      .catch(handleGraphQlErrors);
  },
  getAssigneeQuery() {
    return gpl`
              query GetUsers{
                users{
                  userName
                  email
                }
              }
           `;
  },
  setTitle(ev, el) {
    this.state.currentTask.title = el.value;
  },
  setDescription(ev, el) {
    this.state.currentTask.description = el.value;
  },
  setAssignee(assignee) {
    this.state.currentTask.assignee = first(assignee);
  },
  setTaskType(taskType) {
    this.state.currentTask.taskType = first(taskType);
  },
  setStatus(status) {
    this.state.currentTask.status = first(status);
  },
  setAssignmentGroup(assignmentGroup) {
    this.state.currentTask.assignmentGroup = first(assignmentGroup);
  },
  setupAssigneeComponent() {
    //graphql_endpoint="/api/graph"
    //graphql_query="${state.workTaskFields.assigneeQuery}"
    //graphql_search_query="" collection_name="users"
    //option_label_field="userName"
    //option_value_field="userName" is_multi_select=false
    //is_dynamic_search=true
    //selected_value="${state.currentTask.assignee && state.currentTask.assignee.userName||''}"
    //onChangeEventName="assignee-selected"
    //on-assignee-selected('setAssignee')
    const mountElement = document.getElementById("assignee_id");
    console.log(
      "mountElement",
      mountElement,
      this.els,
      this.el,
      jQuery(this.els).find("#assignee_id")
    );
    let assigneeComponent = customSelect
      .renderSync({
        graphql_endpoint: "/api/graph",
        graphql_query: this.state.workTaskFields.assigneeQuery,
        graphql_search_query: "",
        selected_value:
          (this.state.currentTask.assignee &&
            this.state.currentTask.assignee.userName) ||
            "",
        collection_name: "users",
        option_label_field: "userName",
        option_value_field: "userName",
        is_multi_select: false,
        is_dynamic_search: true,
        onChangeEventName: "assignee-selected"
      })
      .replaceChildrenOf(mountElement)
      .getComponent();

    assigneeComponent.on("assignee-selected", this.setAssignee.bind(this));
  },
  updateWorkTask(state) {
    var router = appContext.get("router");
    client
      .mutate({
        mutation: gpl`
                   mutation UpdateWorkTask($taskId:String!,$input:WorkTaskInput)
                    {
                     updateWorkTask(taskId: $taskId, input:$input) {
                       taskId
                     }
                   }
                  `,
        variables: {
          taskId: state.taskId,
          input: {
            title: state.currentTask.title,
            description: state.currentTask.description,
            requestType: state.currentTask.taskType.id,
            status: state.currentTask.status.id,
            assignee: state.currentTask.assignee.userName,
            group: state.currentTask.assignmentGroup.displayValue
          }
        }
      })
      .then(res => {
        router.redirect("/");
      })
      .catch(handleGraphQlErrors);
  },
  setLoadingTask(isLoading) {
    this.state.isLoading = isLoading;
  },
  setWorkTask(task) {
    var self = this;
    var currentTask = new WorkTask(
      task.taskId,
      task.title,
      task.description,
      task.requestType,
      task.status,
      task.caller,
      task.assignmentGroup,
      task.assignee,
      task.createdAt,
      task.updatedAt
    );
    self.setState("currentTask", currentTask);
  },
  goToList(){
    var router = appContext.get("router");
    router.redirect("/");
  }
};

//query groupMembers($assignmentGroup:String,$page:Int,$limit:Int) {
//  assignmentGroupMembers(assignmentGroup: $assignmentGroup,page:$page,limit:$limit) {
//    group {
//      displayLabel
//      displayValue
//    }
//    members {
//      userName
//      email
//    }
//    pageInfo {
//      total
//      totalPages
//      limit
//      hasNext
//    }
//  }
//}
