import appContext from "../../services/globalAppService";
import gpl from "graphql-tag";
import { client } from "../../services/graphQLServiceApi";
import { handleGraphQlErrors } from "../../utils/errors";
import { first } from "../../utils/helpers";

const STATE_NEW = 4;

export default {
  STATE_NEW: 4,
  createWorkTask(state) {
    var router = appContext.get("router");
    client
      .mutate({
        mutation: gpl`
                   mutation CreateWorkTask($input:WorkTaskInput)
                    {
                     createWorkTask(input:$input) {
                       taskId
                     }
                   }
                  `,
        variables: {
          input: {
            title: state.title,
            description: state.description,
            requestType: state.taskType.id,
            status: this.STATE_NEW,
            caller: state.caller.userName,
            assignee: state.caller.userName
          }
        }
      })
      .then(res => {
        router.redirect("/");
      })
      .catch(handleGraphQlErrors);
  },
  onCreate() {
    this.state = {
      title: "",
      description: "",
      caller: "",
      taskType: "",
      status: this.STATE_NEW,
      workTaskFields: {
        callerQuery: this.getCallerQuery(),
        data: {}
      },
      isLoading: false
    };
    this.getWorkTaskFields();
  },
  onInput(input) {},
  getWorkTaskFields() {
    var self = this;
    self.setLoadingTask(true);
    client
      .query({
        query: gpl`
              query GetFilter {

                status: choiceListByTableAndField(table: "work_task", field: "status") {
                  ...ChoiceListInfo
                }
                requestType:choiceListByTableAndField(table:"work_task",field:"request_type"){
                  ...ChoiceListInfo
                }
              }

              fragment ChoiceListInfo on ChoiceList{
                displayLabel
                displayValue
                id
              }

          `
      })
      .then(promise => {
        self.state.workTaskFields.data = promise.data;
        setTimeout(() => {
          self.setLoadingTask(false);
        });
      })
      .catch(handleGraphQlErrors);
  },
  getCallerQuery() {
    return gpl`
              query GetUsers{
                users{
                  userName
                  email
                }
              }
           `;
  },
  setTitle(ev, el) {
    this.setState("title", el.value);
  },
  setDescription(ev, el) {
    this.setState("description", el.value);
  },
  setCaller(caller) {
    this.setState("caller", first(caller));
  },
  setTaskType(taskType) {
    this.setState("taskType", first(taskType));
  },
  setLoadingTask(isLoading) {
    this.state.isLoading = isLoading;
  }
};
