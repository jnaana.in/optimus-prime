import serviceApi from "../../services/serviceapi";
import workTasksControl from "../work-tasks-control";
import workTasksFilterPanel from "../work-tasks-filter-panel";
import workTasksListView from "../work-tasks-list-view";

export default {
  onCreate() {
    const DEFAULT_SORT_FIELD = {
      displayValue: "created_at",
      displayName: "Created At"
    };
    this.state = {
      selectedSortField: [DEFAULT_SORT_FIELD],
      isOrderbyAscending: false,
      selectedFilterTypes: [],
      selectedCallers: [],
      selectedStatus: []
    };
  },
  sortFieldChangeCallback(selectedSortOption) {
    this.setState("selectedSortField", selectedSortOption);
  },
  orderByChangeCallback(isAscending) {
    this.setState("isOrderbyAscending", isAscending);
  },
  filterTypeChangeCallback(selectedFilterTypes) {
    this.setState(
      "selectedFilterTypes",
      selectedFilterTypes ? selectedFilterTypes : []
    );
  },
  filterStatusChangeCallback(selectedStatus) {
    this.setState("selectedStatus", selectedStatus ? selectedStatus : []);
  },
  callerChangeCallback(selectedCallers) {
    this.setState("selectedCallers", selectedCallers ? selectedCallers : []);
  }
};
