import customSelect from "../custom-select";
import gpl from "graphql-tag";
import { client } from "../../services/graphQLServiceApi";
import { handleGraphQlErrors } from "../../utils/errors";
import WorkTask from "../../models/WorkTask";

export default {
  onCreate() {
    this.state = {
      isLoadingFilterDetails: false,
      staticFilters: {},
      dynamicFilters: {
        callerQuery: this.getCallerQuery()
      }
    };
    this.getStaticFilters();
  },
  onInput() {},
  onSortFieldChangeListener(sortField) {
    this.emit("sort-field-change", sortField);
  },
  onFilterTypeChange(type) {
    this.emit("filter-by-type", type);
  },
  onCallerChange(type) {
    this.emit("filter-by-caller", type);
  },
  onFilterStatusChange(status) {
    this.emit("filter-by-status", status);
  },
  setLoadingTask(isLoading) {
    this.state.isLoadingFilterDetails = isLoading;
  },
  getStaticFilters() {
    var self = this;
    self.setLoadingTask(true);
    client
      .query({
        query: gpl`
              query GetFilter {

                status: choiceListByTableAndField(table: "work_task", field: "status") {
                  ...ChoiceListInfo
                }
                requestType:choiceListByTableAndField(table:"work_task",field:"request_type"){
                  ...ChoiceListInfo
                }
                sortableFields: keyValueListByKey(key:"sortable_field_tasks"){
                  id
                  displayLabel
                  displayValue
                }

              }

              fragment ChoiceListInfo on ChoiceList{
                displayLabel
                displayValue
                id
              }

          `
      })
      .then(promise => {
        self.state.staticFilters = promise.data;
      })
      .then(promise => {
        setTimeout(function() {
          self.setLoadingTask(false);
        }, 300);
      })
      .catch(handleGraphQlErrors);
  },
  getCallerQuery() {
    return gpl`
              query GetUsers{
                users{
                  userName
                  email
                }
              }
           `;
  }
};
