import jQuery from "jquery";
import "Select2";
import cookieUtil from "../../services/cookieUtil";
import { client } from "../../services/graphQLServiceApi";
import { handleGraphQlErrors } from "../../utils/errors";

//set global
window.jQuery = jQuery;

export default {
  fetchData(callback) {
    var self = this;
    self.setFetchingData(false);
    client
      .query({
        query: self.state.graphqlQuery
      })
      .then(promise => {
        self.state.select2Data = promise.data;
      })
      .then(promise => {
        setTimeout(function() {
          self.setFetchingData(false);
        }, 200);
        callback();
      })
      .catch(handleGraphQlErrors);
  },
  onCreate(input) {
    this.state = {
      placeholder: input.placeholder,
      graphqlEndPoint: input.graphql_endpoint,
      graphqlQuery: input.graphql_query || "",
      graphqlSearchQuery: input.graphql_search_query || "",
      select2Data: input.data,
      collectionName: input.collection_name,
      optionLabelField: input.option_label_field,
      optionValueField: input.option_value_field,
      isMultiSelect: input.is_multi_select || false,
      isDynamicSearch: input.is_dynamic_search || false,
      selectedValue: input.selected_value || "",
      eventName: input.onChangeEventName,
      isFetchingData: false
    };
  },
  onMount() {
    this.initSelect();
  },
  initSelect() {
    let self = this;
    let select2Element = self.el.querySelector("select");

    if (self.state.isDynamicSearch === true) {
      self.initSelectWithSearch(select2Element);
    } else {
      self.initStaticSelect(select2Element);
    }
  },
  initSelectWithSearch(select2Element) {
    let self = this;
    self.fetchData(() => {
      let select2Data = self.state.select2Data[
        self.state.collectionName
      ].map(dt => {
        return {
          id: dt[self.state.optionValueField],
          text: dt[self.state.optionLabelField]
        };
      });

      window.jQuery(select2Element).select2({
        placeholder: self.state.placeholder,
        multiple: self.state.isMultiSelect,
        cache: true,
        data: select2Data
      });
      window
        .jQuery(select2Element)
        .val(self.state.selectedValue || "")
        .trigger("change");

      window.jQuery(select2Element).on("change", (event, selectedVal) => {
        var selectedValues = self.findSelectedValue(
          jQuery(event.currentTarget).val()
        );
        self.emit(
          self.state.eventName,
          selectedValues.length > 0 ? selectedValues : null
        );
      });
    });
  },
  initStaticSelect(select2Element) {
    let self = this;
    if (self.state.select2Data) {
      let select2Data = self.state.select2Data.map(dt => {
        return {
          id: dt[self.state.optionValueField],
          text: dt[self.state.optionLabelField]
        };
      });

      window.jQuery(select2Element).select2({
        placeholder: self.state.placeholder,
        multiple: self.state.isMultiSelect,
        cache: true,
        data: select2Data
      });
      window
        .jQuery(select2Element)
        .val(self.state.selectedValue || "")
        .trigger("change");

      window.jQuery(select2Element).on("change", (event, selectedVal) => {
        var selectedValues = self.findSelectedValue(
          jQuery(event.currentTarget).val()
        );
        self.emit(
          self.state.eventName,
          selectedValues.length > 0 ? selectedValues : null
        );
      });
    } else {
      window.jQuery(select2Element).select2({
        placeholder: self.state.placeholder,
        multiple: self.state.isMultiSelect
      });
    }
  },
  setFetchingData(isFetching) {
    this.state.isFetchingData = isFetching;
  },
  findSelectedValue(selectedValue) {
    var self = this;
    var items = self.state.collectionName
      ? self.state.select2Data[self.state.collectionName]
      : self.state.select2Data;

    var filteredItems = items.filter(data => {
      return selectedValue.indexOf(data[self.state.optionValueField]) > -1;
    });
    return filteredItems;
  }
};
