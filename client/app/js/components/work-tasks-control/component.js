export default {
  onCreate(input) {
    this.state = {
      isLoadingTasks: false,
      isOrderbyAscending: typeof input.isOrderbyAscending === "boolean"
        ? input.isOrderbyAscending
        : true
    };
  },
  onInput(input) {},
  onUpdate(input) {},
  setSortOrder(order) {
    var isAscending = order === "ASC";
    this.state.isOrderbyAscending = isAscending;
    this.emit("sort-order-change", isAscending);
  }
};
