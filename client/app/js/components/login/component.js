import { client } from "../../services/graphQLServiceApi";
import appContext from "../../services/globalAppService";
import cookieUtil from "../../services/cookieUtil";
import gpl from "graphql-tag";

export default {
  login() {
    var router = appContext.get("router");
    client
      .mutate({
        mutation: gpl`
                   mutation User($userName: String!,$password: String!) {
                     loginUser(
                       input: {
                          userName:$userName
                          password:$password
                        }
                     )
                   }
                  `,
        variables: {
          userName: this.state.userNameFinal,
          password: this.state.passwordFinal
        }
      })
      .then(res => {
        if (
          res.data.loginUser &&
          res.data.loginUser !== "Authentication Failed"
        ) {
          if (!window.optimus) {
            window.optimus = {};
          }
          cookieUtil.setCookie("X-AUTH-TOKEN", res.data.loginUser);
          appContext.set("X-AUTH-TOKEN", res.data.loginUser);
          router.redirect("/");
        }
      });
  },
  onCreate(input) {
    this.state = {
      userName: "",
      password: "",
      userNameFinal: "",
      passwordFinal: ""
    };
  },
  setPassword(pwd) {
    this.setState("passwordFinal", pwd.target.value);
  },
  setUserName(userName) {
    this.setState("userNameFinal", userName.target.value);
  }
};
