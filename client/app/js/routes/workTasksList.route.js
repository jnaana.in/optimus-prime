import workTasks from "../components/work-tasks";
var mountElement = document.getElementById("root");
import routeHelper from "./routeHelper.js";

export default {
  setupComponent: function() {
    routeHelper.initSetup(mountElement, () => {
      var component = workTasks
        .renderSync({})
        .replaceChildrenOf(document.getElementById("root"))
        .getComponent();
      window.optimus.components.push(component);
    });
  }
};
