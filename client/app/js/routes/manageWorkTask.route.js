import manageWorkTask from "../components/manage-work-tasks";
var mountElement = document.getElementById("root");
import routeHelper from "./routeHelper.js";

export default {
  setupComponent: function(param, query) {
    routeHelper.initSetup(mountElement, () => {
      var component = manageWorkTask
        .renderSync({
          params: param,
          query: query
        })
        .replaceChildrenOf(document.getElementById("root"))
        .getComponent();
      window.optimus.components.push(component);
    });
  }
};
//   How to set up async component;
//      var resultPromise = manageWorkTask.render({
//        params: param,
//        query: query
//      });
//      resultPromise.then(result => {
//        result.appendTo(document.getElementById("root"));
//      });
