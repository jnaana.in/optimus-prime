import createWorkTask from "../components/create-work-tasks";
var mountElement = document.getElementById("root");
import routeHelper from "./routeHelper.js";

export default {
  setupComponent: function() {
    routeHelper.initSetup(mountElement, () => {
      var component = createWorkTask
        .renderSync({})
        .replaceChildrenOf(document.getElementById("root"))
        .getComponent();
      window.optimus.components.push(component);
    });
  }
};
