import login from "../components/login";
var mountElement = document.getElementById("root");
import routeHelper from "./routeHelper.js";

export default {
  setupComponent: function() {
    routeHelper.initSetup(mountElement, () => {
      var component = login
        .renderSync({})
        .replaceChildrenOf(mountElement)
        .getComponent();
      window.optimus.components.push(component);
    });
  }
};
