export default {
  initSetup: (mountElement, setComponentCallback) => {
    if (mountElement) {
      while (mountElement.firstChild)
        mountElement.removeChild(mountElement.firstChild);

      var node = document.getElementsByClassName("select2-container");
      var keys = Object.keys(node);
      for (var i = 0; i < keys.length; i++) {
        while (node[i].firstChild) {
          node[i].removeChild(node[i].firstChild);
        }
      }

      for (var j = 0; j < window.optimus.components.length; j++) {
        window.optimus.components[j].destroy();
      }

      window.optimus.components = [];
      if (typeof setComponentCallback === "function") {
        setComponentCallback();
      }
    }
  }
};
