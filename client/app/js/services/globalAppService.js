var appSingleton = {
  appContext: (function() {
    // instance variable to hold the reference of Object.
    var instance;

    function initialize() {
      // private members here
      var _this = this;
      var secretKey = +new Date() + "" + parseInt(Math.random() * 1000, 10);
      var sharedItems = {};
      return {
        // public members.
        getSecretKey: function() {
          return secretKey;
        },
        set: function(key, val) {
          sharedItems[key] = val;
        },
        get: function(key) {
          return sharedItems[key];
        }
      };
    }

    return {
      getInstance: function() {
        if (!instance) {
          instance = initialize();
        }
        return instance;
      }
    };
  })()
};

export default appSingleton.appContext.getInstance();
