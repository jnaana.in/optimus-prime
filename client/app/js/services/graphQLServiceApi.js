import ApolloClient, { createNetworkInterface } from "apollo-client";
import cookieUtil from "./cookieUtil";

const networkInterface = createNetworkInterface({ uri: "/api/graph" });

networkInterface.use([
  {
    applyMiddleware(req, next) {
      if (!req.options.headers) {
        req.options.headers = {}; // Create the header object if needed.
      }
      req.options.headers["X-AUTH-TOKEN"] = cookieUtil.readCookie(
        "X-AUTH-TOKEN"
      );
      next();
    }
  }
]);

export const client = new ApolloClient({ networkInterface });
