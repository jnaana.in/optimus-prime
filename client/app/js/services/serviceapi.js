import cookieUtil from "./cookieUtil";
import "whatwg-fetch";

let fetch = window.fetch;

function queryParams(params) {
  return Object.keys(params)
    .map(k => encodeURIComponent(k) + "=" + encodeURIComponent(params[k]))
    .join("&");
}

export default {
  get: (url, options) => {
    let self = this;
    let opts = {
      method: "GET",
      headers: {
        "Content-Type": "application/json",
        "X-AUTH-TOKEN": cookieUtil.readCookie("X-AUTH-TOKEN")
      },
      credentials: "same-origin"
    };

    if (options.queryParams) {
      url +=
        (url.indexOf("?") === -1 ? "?" : "&") +
        queryParams(options.queryParams);
      delete options.queryParams;
    }

    for (let key in options) {
      opts[key] = options[key];
    }

    return fetch(url, opts).then(
      response => {
        return response.json();
      },
      error => {
        console.log("Error Message", error);
        return error.message;
      }
    );
  },
  post: (url, options) => {
    let opts = {
      method: "POST",
      headers: {
        "Content-Type": "application/json",
        "X-AUTH-TOKEN": cookieUtil.readCookie("X-AUTH-TOKEN")
      }
    };

    for (let key in options) {
      opts[key] = options[key];
    }

    return fetch(url, opts).then(
      response => {
        return response.json();
      },
      error => {
        return error.message;
      }
    );
  },
  put: (url, options) => {
    let opts = {
      method: "PUT",
      headers: {
        "Content-Type": "application/json",
        "X-AUTH-TOKEN": cookieUtil.readCookie("X-AUTH-TOKEN")
      }
    };

    for (let key in options) {
      opts[key] = options[key];
    }

    return fetch(url, opts).then(
      response => {
        return response.json();
      },
      error => {
        return error.message;
      }
    );
  },
  delete: (url, options) => {
    let self = this;
    let opts = {
      method: "DELETE",
      headers: {
        "Content-Type": "application/json",
        "X-AUTH-TOKEN": cookieUtil.readCookie("X-AUTH-TOKEN")
      },
      credentials: "same-origin"
    };

    if (options.queryParams) {
      url +=
        (url.indexOf("?") === -1 ? "?" : "&") +
        queryParams(options.queryParams);
      delete options.queryParams;
    }

    for (let key in options) {
      opts[key] = options[key];
    }

    return fetch(url, opts).then(
      response => {
        return response;
      },
      error => {
        return error.message;
      }
    );
  }
};
