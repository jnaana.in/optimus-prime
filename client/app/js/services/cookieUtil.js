var cookieUtil = {
  context: (function() {
    // instance variable to hold the reference of Object.
    var instance;

    function initialize() {
      // private members here
      var _this = this;
      var cookies;
      return {
        readCookie: function readCookie(name) {
          var value = "; " + document.cookie;
          var parts = value.split("; " + name + "=");
          if (parts.length == 2) return parts.pop().split(";").shift();
        },
        setCookie: function setCookie(cookieName, cookieValue, nMinutes) {
          var today = new Date();
          var expire = new Date();
          if (nMinutes == null || nMinutes == 0) nMinutes = 15;
          expire.setTime(today.getTime() + nMinutes * 60 * 1000);
          var cookieVal =
            cookieName +
            "=" +
            escape(cookieValue) +
            ";expires=" +
            expire.toGMTString();
          document.cookie = cookieVal;
          console.log("cookie", cookieVal);
        },
        deleteCookie: function deleteCookie(cookieName) {
          this.setCookie(cookieName, "", -5000);
        }
      };
    }

    return {
      getInstance: function() {
        if (!instance) {
          instance = initialize();
        }
        return instance;
      }
    };
  })()
};

export default cookieUtil.context.getInstance();
