import page from "page";
import appContext from "./services/globalAppService";
import cookieUtil from "./services/cookieUtil";
import loginRoute from "./routes/login.route";
import createWorkTask from "./routes/createWorkTask.route";
import manageWorkTask from "./routes/manageWorkTask.route";
import workTasks from "./routes/workTasksList.route";
import { redirectToLogin } from "./utils/actions";
var useHash = true;

window.optimus = window.optimus || {};
window.optimus.components = [];

/**
 * Check Login
 * @param next
 */
function checkLogin(context, next) {
  var apiToken = cookieUtil.readCookie("X-AUTH-TOKEN");
  if (apiToken) {
    next();
  } else {
    redirectToLogin();
  }
}

/**
 * Remove JWT Token
 */
function invalidateCookie(context, next) {
  cookieUtil.deleteCookie("X-AUTH-TOKEN");
  next();
}

function enableLogout(context, next) {
  var logout = document.getElementById("logout");
  var isLoginOrLogout =
    context.pathname === "/login" || context.pathname === "/logout";
  logout.style.display = isLoginOrLogout ? "none" : "block";
  next();
}

// Router Setup
page("/", checkLogin, enableLogout, workTasks.setupComponent);
page("/create-task", checkLogin, enableLogout, createWorkTask.setupComponent);
page("/login", enableLogout, loginRoute.setupComponent);
page("/logout", invalidateCookie, enableLogout, loginRoute.setupComponent);
page("/work-task/:id", checkLogin, enableLogout, manageWorkTask.setupComponent);
page.start({ hashbang: useHash });
appContext.set("router", page);
