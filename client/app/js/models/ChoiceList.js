/*
{
   "id":4,
   "tableName":"work_task",
   "fieldName":"status",
   "displayLabel":"New",
   "displayValue":"new",
   "sortOrder":1,
   "createdAt":"2017-03-30T19:43:57.000+0000",
   "updatedAt":"2017-03-30T19:43:57.000+0000"
}
*/

function ChoiceList(
  id,
  tableName,
  fieldName,
  displayLabel,
  displayValue,
  sortOrder,
  createdAt,
  updatedAt
) {
  this.id = id;
  this.tableName = tableName;
  this.fieldName = fieldName;
  this.displayLabel = displayLabel;
  this.displayValue = displayValue;
  this.sortOrder = sortOrder;
  this.createdAt = createdAt;
  this.updatedAt = updatedAt;
}

export default ChoiceList;
