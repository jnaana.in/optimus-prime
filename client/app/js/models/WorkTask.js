/*
{
   "taskId":"TSK100000",
   "title":"senthilpk@ymail.com",
   "description":"sairam",
   "taskType":{
      "id":1,
      "tableName":"work_task",
      "fieldName":"task_type",
      "displayLabel":"Request",
      "displayValue":"request",
      "sortOrder":1,
      "createdAt":null,
      "updatedAt":null
   },
   "status":{
      "id":4,
      "tableName":"work_task",
      "fieldName":"status",
      "displayLabel":"New",
      "displayValue":"new",
      "sortOrder":1,
      "createdAt":"2017-03-30T19:43:57.000+0000",
      "updatedAt":"2017-03-30T19:43:57.000+0000"
   },
   "createdAt":"2017-02-26T20:59:32.000+0000",
   "updatedAt":"2017-02-26T20:59:32.000+0000"
}
*/

import ChoiceList from "./ChoiceList";

import User from "./User";
import AssignmentGroup from "./AssignmentGroup";

function WorkTask(
  taskId,
  title,
  description,
  taskType,
  status,
  caller,
  assignmentGroup,
  assignee,
  createdAt,
  updatedAt
) {
  this.taskId = taskId;
  this.title = title;
  this.description = description;
  this.taskType = new ChoiceList(
    taskType.id,
    taskType.table,
    taskType.field,
    taskType.displayLabel,
    taskType.displayValue,
    taskType.sortOrder,
    taskType.createdAt,
    taskType.updatedAt
  );
  this.status = new ChoiceList(
    status.id,
    status.table,
    status.field,
    status.displayLabel,
    status.displayValue,
    status.sortOrder,
    status.createdAt,
    status.updatedAt
  );
  this.caller = caller ? new User(caller.userName, caller.email) : "";
  this.assignee = assignee ? new User(assignee.userName, assignee.email) : "";
  this.assignmentGroup = assignmentGroup
    ? new AssignmentGroup(
        assignmentGroup.displayLabel,
        assignmentGroup.displayValue,
        assignmentGroup.owner
      )
    : "";
  this.createdAt = createdAt;
  this.updatedAt = updatedAt;
  this.isCurrent = false;
}

export default WorkTask;
