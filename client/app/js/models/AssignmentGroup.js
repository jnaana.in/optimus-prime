function AssignmentGroup(displayLabel, displayValue, owner) {
  this.displayLabel = displayLabel;
  this.displayValue = displayValue;
  this.owner = owner;
}

export default AssignmentGroup;
