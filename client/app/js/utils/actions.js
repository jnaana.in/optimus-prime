import page from "page";

export const redirectToLogin = () => {
  page.redirect("/login");
};
