import { redirectToLogin } from "./actions";

export const handleGraphQlErrors = error => {
  if (
    error.message === "GraphQL error: jwt expired" ||
    error.message === "GraphQL error: jwt malformed"
  ) {
    redirectToLogin();
  }
};
