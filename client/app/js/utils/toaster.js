import Noty from "noty";

export const showInfoMessage = message => {
  new Noty({
    type: "info",
    layout: "topRight",
    timeout: 1000000000,
    text: message
  }).show();
};
