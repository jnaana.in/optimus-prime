/**
 * Sorting GraphQL Type
 */
export const SortOrderType = `

input SortOrder{
  field: String
  direction: String
}

`;
