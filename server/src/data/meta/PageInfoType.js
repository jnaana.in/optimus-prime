/**
 * Pagination GraphQL Type
 */
export const PageInfoType = `

type PageInfo{
  total: Int
  totalPages:Int
  limit:Int
  hasNext: Boolean
}

`;
