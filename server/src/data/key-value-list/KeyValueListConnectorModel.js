import Model from "../KnexModel";

export default class KeyValueListConnectorModel extends Model {
  static tableName = "key_value_list";

  // JsonSchema to validate data.
  static jsonSchema = {
    type: "object",
    required: ["id", "display_label", "display_value", "key"],
    properties: {
      key: { type: "string" },
      display_value: { type: "string" },
      display_label: { type: "string" },
      created_at: { type: "date" },
      updated_at: { type: "date" }
    }
  };

  $beforeInsert() {
    this.created_at = new Date().toISOString();
  }

  $beforeUpdate() {
    this.updated_at = new Date().toISOString();
  }
}
