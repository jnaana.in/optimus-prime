/**
 * KeyValueList Model
 */
export default class KeyValueListModel {
  constructor(id, key, displayLabel, displayValue) {
    this.id = id;
    this.key = key;
    this.displayLabel = displayLabel;
    this.displayValue = displayValue;
  }
}
