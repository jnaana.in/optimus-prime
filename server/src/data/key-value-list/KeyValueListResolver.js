import KeyValueListConnectorModel from "./KeyValueListConnectorModel";
import KeyValueListModel from "./KeyValueListModel";

/**
 * Get KeyValue list
 * @param args
 * @returns {*}
 */
export const getKeyValueList = (args, context) => {
  return KeyValueListConnectorModel.query()
    .where("key", args.key)
    .orderBy("sort_order", "asc")
    .then(onKeyValueListQuery);
};

const onKeyValueListQuery = keyValues => {
  if (keyValues.length > 0) {
    return keyValues.map(keyValue => {
      return new KeyValueListModel(
        keyValue.id,
        keyValue.key,
        keyValue.display_label,
        keyValue.display_value
      );
    });
  }
  return null;
};
