/**
 * GraphQL KeyValueListType
 */
export const KeyValueListType = `

type KeyValueList {
  id: ID!
  table: String
  field: String
  displayLabel: String
  displayValue: String
}

`;
