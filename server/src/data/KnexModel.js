import { Model } from "objection";
import Knex from "knex";
import KnexConfig from "../../knexfile";

// Initialize knex connection.
const knex = Knex(KnexConfig.production);
// Give the connection to objection.
Model.knex(knex);

export default Model;
