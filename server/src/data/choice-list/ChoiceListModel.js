/**
 * ChoiceList Model
 */
export default class ChoiceListModel {
  constructor(id, table, field, displayLabel, displayValue) {
    this.id = id;
    this.table = table;
    this.field = field;
    this.displayLabel = displayLabel;
    this.displayValue = displayValue;
  }
}
