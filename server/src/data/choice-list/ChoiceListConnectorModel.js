import Model from "../KnexModel";

/**
 * Choice List Connector model
 */
export default class ChoiceListConnectorModel extends Model {
  static tableName = "choice_list";

  // JsonSchema to validate data.
  static jsonSchema = {
    type: "object",
    required: ["id", "display_label", "display_value", "table", "field"],
    properties: {
      table: { type: "string" },
      field: { type: "string" },
      display_value: { type: "string" },
      display_label: { type: "string" },
      created_at: { type: "date" },
      updated_at: { type: "date" }
    }
  };

  $beforeInsert() {
    this.created_at = new Date().toISOString();
  }

  $beforeUpdate() {
    this.updated_at = new Date().toISOString();
  }
}
