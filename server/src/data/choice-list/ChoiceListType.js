/**
 * GraphQL ChoiceListType
 */
export const ChoiceListType = `

type ChoiceList {
  id: ID!
  table: String
  field: String
  displayLabel: String
  displayValue: String
}

`;
