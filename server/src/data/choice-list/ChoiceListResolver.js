import ChoiceListConnectorModel from "./ChoiceListConnectorModel";
import ChoiceListModel from "./ChoiceListModel";

/**
 * ChoiceList resolver to be used in GraphQL rootValue
 * @param args
 * @returns {*}
 */
export const getChoiceList = (args, context) => {
  return ChoiceListConnectorModel.query()
    .where("table", args.table)
    .andWhere("field", args.field)
    .orderBy("sort_order", "asc")
    .then(onQueryChoiceList);
};

const onQueryChoiceList = choices => {
  if (choices.length > 0) {
    return choices.map(choice => {
      return new ChoiceListModel(
        choice.id,
        choice.table,
        choice.field,
        choice.display_label,
        choice.display_value
      );
    });
  }
  return null;
};
