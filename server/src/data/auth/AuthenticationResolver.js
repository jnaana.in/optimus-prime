import { skip } from "graphql-resolvers";
import jwt from "jsonwebtoken";

/**
 * Authenticate the user
 *
 */
export const isAuthenticated = (args, context) => {
  if (context.headers["x-auth-token"]) {
    try {
      jwt.verify(context.headers["x-auth-token"], process.env.APP_SECRET);
    } catch (err) {
      return err;
    }
    return skip;
  }
  return new Error("Authentication Token is missing");
};
