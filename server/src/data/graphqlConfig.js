import { buildSchema } from "graphql";
import { combineResolvers } from "graphql-resolvers";
import { UserType } from "./user/UserType";
import {
  getUsers,
  getUser,
  registerUser,
  loginUser
} from "./user/UserResolver";
import { ChoiceListType } from "./choice-list/ChoiceListType";
import { getChoiceList } from "./choice-list/ChoiceListResolver";
import { KeyValueListType } from "./key-value-list/KeyValueListType";
import { getKeyValueList } from "./key-value-list/KeyValueListResolver";
import { AssignmentGroupType } from "./assignment-group/AssignmentGroupType";
import {
  getAssignmentGroups,
  getAssignmentGroupByKey
} from "./assignment-group/AssignmentGroupResolver";
import { GroupMemberType } from "./group-member/GroupMemberType";
import { getGroupMembers } from "./group-member/GroupMemberResolver";
import { WorkTaskType } from "./work-task/WorkTaskType";
import {
  getWorkTasks,
  getWorkTask,
  createWorkTask,
  updateWorkTask,
  deleteWorkTask
} from "./work-task/WorkTaskResolver";
import { isAuthenticated } from "./auth/AuthenticationResolver";
import { PageInfoType } from "./meta/PageInfoType";
import { SortOrderType } from "./meta/SortOrderType";

const QueryType = `
# Root Query
type Query {
  user(userName: String): User
  users: [User]
  choiceListByTableAndField(table:String,field:String): [ChoiceList]
  keyValueListByKey(key:String): [KeyValueList]
  assignmentGroups:[AssignmentGroup],
  assignmentGroupByKey(assignmentGroup: String):AssignmentGroupByKey,
  assignmentGroupMembers(assignmentGroup: String,page:Int,limit:Int):AssignmentGroupMembers
  workTasks(page:Int,limit:Int,orderBy:SortOrder,filters:WorkTaskFilters): WorkTaskList
  workTask(taskId: String): WorkTask
}

`;

const Mutation = `
  type Mutation{
    registerUser(input: NewUserInput): User
    loginUser(input: UserInput): String
    createWorkTask(input: WorkTaskInput): WorkTask
    updateWorkTask(taskId:String,input: WorkTaskInput): WorkTask
    deleteWorkTask(taskId:String): String
  }
`;

const schema = [
  QueryType,
  PageInfoType,
  SortOrderType,
  UserType,
  ChoiceListType,
  KeyValueListType,
  AssignmentGroupType,
  GroupMemberType,
  WorkTaskType,
  Mutation
].join("\n");

const resolvers = {
  users: combineResolvers(isAuthenticated, getUsers),
  user: combineResolvers(isAuthenticated, getUser),
  registerUser: registerUser,
  loginUser: loginUser,
  choiceListByTableAndField: combineResolvers(isAuthenticated, getChoiceList),
  keyValueListByKey: combineResolvers(isAuthenticated, getKeyValueList),
  assignmentGroups: combineResolvers(isAuthenticated, getAssignmentGroups),
  assignmentGroupByKey: combineResolvers(
    isAuthenticated,
    getAssignmentGroupByKey
  ),
  assignmentGroupMembers: combineResolvers(isAuthenticated, getGroupMembers),
  workTasks: combineResolvers(isAuthenticated, getWorkTasks),
  workTask: combineResolvers(isAuthenticated, getWorkTask),
  createWorkTask: combineResolvers(isAuthenticated, createWorkTask),
  updateWorkTask: combineResolvers(isAuthenticated, updateWorkTask),
  deleteWorkTask: combineResolvers(isAuthenticated, deleteWorkTask)
};
// GraphQL Configuration provided to Server
export const config = {
  schema: buildSchema(schema),
  rootValue: resolvers
};
