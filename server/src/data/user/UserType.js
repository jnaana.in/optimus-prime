/**
 * GraphQL User Type
 */
export const UserType = `
type User{
  userName:String
  email: String
}

input NewUserInput{
  userName: String
  email: String
  password: String
}

input UserInput{
  userName: String
  password: String
}

`;
