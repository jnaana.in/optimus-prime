import Model from "../KnexModel";
/**
 * User Model connects to DB
 */
export default class UserConnectorModel extends Model {
  static tableName = "users";
  static idColumn = "username";

  // JsonSchema to validate data.
  static jsonSchema = {
    type: "object",
    required: ["username", "email", "password"],
    properties: {
      username: { type: "string" },
      email: { type: "string" },
      password: { type: "string" },
      created_at: { type: "date" },
      updated_at: { type: "date" }
    }
  };

  $beforeInsert() {
    this.created_at = new Date().toISOString();
    this.updated_at = new Date().toISOString();
  }

  $beforeUpdate() {
    this.updated_at = new Date().toISOString();
  }
}
