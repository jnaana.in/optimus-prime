/*
 *
 *  User Model
 *
 * */
export default class UserModel {
  constructor(userName, email) {
    this.userName = userName;
    this.email = email;
  }
}
