import UserConnectorModel from "./UserConnectorModel";
import UserModel from "./UserModel";
import bcrypt from "bcrypt-nodejs";
import jwt from "jsonwebtoken";

/**
 * Get users
 * @param args
 * @returns {*}
 */
export const getUsers = (args, context) => {
  return UserConnectorModel.query().then(users => users.map(generateUser));
};

/**
 * Get user
 * @param args
 * @returns {*}
 */
export const getUser = (args, context) => {
  return UserConnectorModel.query().findById(args.userName).then(generateUser);
};

/**
 * Register a new user
 * @param args
 * @param context
 */
export const registerUser = (args, context) => {
  var { userName, email, password } = args.input;
  return UserConnectorModel.query()
    .insert({
      username: userName,
      email: email,
      password: generateHash(password)
    })
    .then(generateUser);
};

/**
 * Login user and store the user info in Context(request)
 * @param args
 * @param context
 * @returns {Promise}
 */
export const loginUser = (args, context) => {
  var { userName, password } = args.input;
  return UserConnectorModel.query().findById(userName).then(user => {
    return validateUserAndCreateToken(user, password, context);
  });
};

const generateHash = password =>
  bcrypt.hashSync(password, bcrypt.genSaltSync(8), null);

const validPassword = (inputPassword, password) =>
  bcrypt.compareSync(inputPassword, password);

const generateUser = user => {
  if (user) {
    return new UserModel(user.username, user.email);
  }
  return null;
};

const validateUserAndCreateToken = (user, inputPassword, context) => {
  if (user && validPassword(inputPassword, user.password)) {
    let token = jwt.sign(user, process.env.APP_SECRET, {
      expiresIn: 1800 // in seconds
    });
    return token;
  }
  return "Authentication Failed";
};
