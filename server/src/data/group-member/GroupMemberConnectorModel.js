import Model from "../KnexModel";
import UserConnectorModel from "../user/UserConnectorModel";
import AssignmentGroupConnectorModel from "../assignment-group/AssignmentGroupConnectorModel";

/**
 * Assignment Group M2M relation
 */
export default class GroupMemberConnectorModel extends Model {
  static tableName = "assignment_group_members";

  static jsonSchema = {
    required: ["group", "member"],
    properties: {
      group: { type: "string" },
      member: { type: "string" },
      created_at: { type: "date" },
      updated_at: { type: "date" }
    }
  };

  static relationMappings = {
    assignmentGroup: {
      relation: Model.BelongsToOneRelation,
      modelClass: AssignmentGroupConnectorModel,
      join: {
        from: "assignment_group_members.group",
        to: "assignment_group.display_value"
      }
    },
    groupMember: {
      relation: Model.BelongsToOneRelation,
      modelClass: UserConnectorModel,
      join: {
        from: "assignment_group_members.member",
        to: "users.username"
      }
    }
  };

  $beforeInsert() {
    this.created_at = new Date().toISOString();
    this.updated_at = new Date().toISOString();
  }

  $beforeUpdate() {
    this.updated_at = new Date().toISOString();
  }
}
