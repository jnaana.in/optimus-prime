import GroupMemberConnectorModel from "./GroupMemberConnectorModel";
import GroupMemberModel from "./GroupMemberModel";
import AssignmentGroupModel from "../assignment-group/AssignmentGroupModel";
import UserModel from "../user/UserModel";

export const getGroupMembers = (args, context) => {
  const page = args.page ? args.page - 1 : 1;

  if (page < 0) {
    return null;
  }

  let query = GroupMemberConnectorModel.query()
    .eager("[groupMember,assignmentGroup]")
    .mergeEager("assignmentGroup.[groupOwner]")
    .joinRelation("groupMember", { alias: "gm" })
    .leftJoinRelation("assignmentGroup", { alias: "ag" });

  query
    .whereIn("ag.display_value", args.assignmentGroup)
    .orderBy("group", "asc")
    .page(page, args.limit);

  return query.then(({ results, total }) => {
    return onQueryGroupMembers(results, total, {
      page: page,
      limit: args.limit
    });
  });
};

const onQueryGroupMembers = (groupMembers, total, args) => {
  var groups = {};

  for (var i = 0; i <= groupMembers.length - 1; i++) {
    const groupMember = groupMembers[i];
    const ag = groupMember.assignmentGroup;
    const mem = groupMember.groupMember;
    var mod;

    if (groups[groupMember.group]) {
      mod = groups[groupMember.group];
      mod.addMember(new UserModel(mem.username, mem.email));
    } else {
      mod = new GroupMemberModel(
        ag
          ? new AssignmentGroupModel(
              ag.display_label,
              ag.display_value,
              ag.groupOwner
                ? new UserModel(ag.groupOwner.username, ag.groupOwner.email)
                : null
            )
          : null
      );

      mod.addMember(new UserModel(mem.username, mem.email));
      groups[groupMember.group] = mod;
    }
  }
  const totalPages = Math.round(total / args.limit);
  const hasNext = totalPages > args.page;
  const limit = args.limit;
  const pageInfo = { total, totalPages, hasNext, limit };

  var keys = Object.keys(groups);
  if (keys.length > 0) {
    var finalGroup = groups[keys[0]];
    finalGroup.setPageInfo(pageInfo);
    return finalGroup;
  }

  return null;
};
