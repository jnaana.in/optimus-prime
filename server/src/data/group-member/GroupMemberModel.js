/**
 * AssignmentGroup Model
 */
export default class GroupMemberModel {
  constructor(group) {
    this.group = group;
    this.members = [];
    this.pageInfo = {};
  }

  addMember(member) {
    this.members.push(member);
  }

  setMembers(members) {
    this.members = members;
  }

  setPageInfo(pageInfo) {
    this.pageInfo = pageInfo;
  }
}
