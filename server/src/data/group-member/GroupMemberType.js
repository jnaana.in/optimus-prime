/**
 * GraphQL GroupMemberType
 */
export const GroupMemberType = `

type AssignmentGroupMembers {
  members: [User]
  group: AssignmentGroup
  pageInfo: PageInfo
}

`;
