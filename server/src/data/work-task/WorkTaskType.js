/**
 * GraphQL WorkTask type
 */
export const WorkTaskType = `

type WorkTask{
  taskId: String
  title: String
  description: String
  caller: User
  assignmentGroup: AssignmentGroup
  assignee: User
  requestType: ChoiceList
  status: ChoiceList
  createdAt: String
  updatedAt: String
}

type WorkTaskList{
  nodes:[WorkTask]
  pageInfo: PageInfo
}

input WorkTaskInput{
  title: String
  description: String
  caller: String
  assignee: String
  group: String
  requestType: Int
  status: Int
}

input WorkTaskFilters{
  requestType:[String]
  status: [String]
  caller:[String]
  assignee:[String]
}

`;
