import WorkTaskConnectorModel from "./WorkTaskConnectorModel";
import WorkTaskModel from "./WorkTaskModel";
import ChoiceListModel from "../choice-list/ChoiceListModel";
import UserModel from "../user/UserModel";
import AssignmentGroupModel from "../assignment-group/AssignmentGroupModel";

/**
 * Get Work tasks
 * @param args
 * @returns {*}
 */
export const getWorkTasks = (args, context) => {
  let query = WorkTaskConnectorModel.query()
    .eager("[calledUser, assignedUser, requestType, status,assignmentGroup]")
    .joinRelation("requestType", { alias: "rt" })
    .joinRelation("status", { alias: "status" })
    .leftJoinRelation("calledUser", { alias: "caller" })
    .leftJoinRelation("assignedUser", { alias: "assignee" })
    .leftJoinRelation("assignmentGroup", { alias: "assGroup" });

  applyRequestTypeFilter(args.filters, query);
  applyStatusFilter(args.filters, query);
  applyCallerFilter(args.filters, query);
  applyAssigneeFilter(args.filters, query);

  query
    .orderBy(args.orderBy.field, args.orderBy.direction)
    .page(args.page, args.limit);

  return query.then(({ results, total }) => {
    const totalPages = Math.round(total / args.limit);
    const hasNext = totalPages > args.page;
    const limit = args.limit;
    return {
      nodes: results.map(generateWorkTask),
      pageInfo: { total, totalPages, hasNext, limit }
    };
  });
};

/**
 *Get work task
 * @param args
 * @returns {*}
 */
export const getWorkTask = (args, context) => {
  return WorkTaskConnectorModel.query()
    .eager("[calledUser, assignedUser, requestType, status,assignmentGroup]")
    .findById(args.taskId)
    .then(generateWorkTask);
};

/**
 * Create Work Task
 * @param args
 */
export const createWorkTask = (args, context) => {
  return WorkTaskConnectorModel.query()
    .eager("[calledUser, assignedUser, requestType, status,assignmentGroup]")
    .insert({
      title: args.input.title,
      description: args.input.description,
      caller: args.input.caller,
      assignee: args.input.assignee ? args.input.assignee : null,
      task_type_id: args.input.requestType,
      status_id: args.input.status
    })
    .then(generateWorkTask);
};

/**
 * Updates Work Task
 * @param args
 * @returns {Promise}
 */
export const updateWorkTask = (args, context) => {
  return WorkTaskConnectorModel.query()
    .eager("[calledUser, assignedUser, requestType, status,assignmentGroup]")
    .skipUndefined()
    .patchAndFetchById(args.taskId, {
      title: args.input.title,
      description: args.input.description,
      caller: args.input.caller,
      assignee: args.input.assignee,
      task_type_id: args.input.requestType,
      status_id: args.input.status,
      group: args.input.group
    })
    .then(generateWorkTask);
};

/**
 * Deletes a Work Task
 * @param args
 */
export const deleteWorkTask = (args, context) => {
  return WorkTaskConnectorModel.query()
    .deleteById(args.taskId)
    .then(noOfRowsDelete => {
      if (noOfRowsDelete === 1) {
        return "ok";
      }
      return "something went wrong";
    });
};

const generateWorkTask = task => {
  if (task) {
    return new WorkTaskModel(
      task.task_id,
      task.title,
      task.description,
      task.calledUser !== null
        ? new UserModel(task.calledUser.username, task.calledUser.email)
        : task.calledUser,
      task.assignmentGroup !== null
        ? new AssignmentGroupModel(
            task.assignmentGroup.display_label,
            task.assignmentGroup.display_value,
            task.assignmentGroup.owner
          )
        : null,
      task.assignedUser !== null
        ? new UserModel(task.assignedUser.username, task.assignedUser.email)
        : task.assignedUser,
      new ChoiceListModel(
        task.requestType.id,
        task.requestType.table,
        task.requestType.field,
        task.requestType.display_label,
        task.requestType.display_value
      ),
      new ChoiceListModel(
        task.status.id,
        task.status.table,
        task.status.field,
        task.status.display_label,
        task.status.display_value
      ),
      task.created_at,
      task.updated_at
    );
  }
  return null;
};

const applyRequestTypeFilter = (filters, query) => {
  if (
    typeof filters.requestType !== "undefined" &&
    filters.requestType.length > 0
  ) {
    query.whereIn("rt.display_value", filters.requestType);
    query.where("rt.table", "work_task");
    query.where("rt.field", "request_type");
  }
};

const applyStatusFilter = (filters, query) => {
  if (typeof filters.status !== "undefined" && filters.status.length > 0) {
    query.whereIn("status.display_value", filters.status);
    query.where("status.table", "work_task");
    query.where("status.field", "status");
  }
};

const applyCallerFilter = (filters, query) => {
  if (typeof filters.caller !== "undefined" && filters.caller.length > 0) {
    query.whereIn("caller", filters.caller);
  }
};

const applyAssigneeFilter = (filters, query) => {
  if (typeof filters.assignee !== "undefined" && filters.assignee.length > 0) {
    query.whereIn("assignee.username", filters.assignee);
  }
};
