/*
*
* Work Task Model
* */

export default class WorkTaskModel {
  constructor(
    taskId,
    title,
    description,
    caller,
    assignmentGroup,
    assignee,
    requestType,
    status,
    createdAt,
    updatedAt
  ) {
    this.taskId = taskId;
    this.title = title;
    this.description = description;
    this.assignmentGroup = assignmentGroup;
    this.assignee = assignee;
    this.caller = caller;
    this.requestType = requestType;
    this.status = status;
    this.createdAt = createdAt;
    this.updatedAt = updatedAt;
  }
}
