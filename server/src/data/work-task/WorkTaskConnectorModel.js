import Model from "../KnexModel";
import ChoiceListConnectorModel from "../choice-list/ChoiceListConnectorModel";
import UserConnectorModel from "../user/UserConnectorModel";
import AssignmentGroup from "../assignment-group/AssignmentGroupConnectorModel";

/**
 * Work Tasks connector model
 */
export default class WorkTaskConnectorModel extends Model {
  static tableName = "work_tasks";
  static pickJsonSchemaProperties = false;
  static idColumn = "task_id";
  static jsonSchema = {
    type: "object",
    required: [],
    properties: {
      task_id: { type: "string" },
      title: { type: "string" },
      description: { type: "string" },
      caller: { type: "string" },
      group: { type: "string" },
      assignee: { type: "string" },
      task_type_id: { type: "integer" },
      status_id: { type: "integer" },
      created_at: { type: "date" },
      updated_at: { type: "date" }
    }
  };

  static relationMappings = {
    calledUser: {
      relation: Model.HasOneRelation,
      modelClass: UserConnectorModel,
      join: {
        from: "work_tasks.caller",
        to: "users.username"
      }
    },
    assignmentGroup: {
      relation: Model.HasOneRelation,
      modelClass: AssignmentGroup,
      join: {
        from: "work_tasks.group",
        to: "assignment_group.display_value"
      }
    },
    assignedUser: {
      relation: Model.HasOneRelation,
      modelClass: UserConnectorModel,
      join: {
        from: "work_tasks.assignee",
        to: "users.username"
      }
    },
    requestType: {
      relation: Model.HasOneRelation,
      modelClass: ChoiceListConnectorModel,
      join: {
        from: "work_tasks.task_type_id",
        to: "choice_list.id"
      }
    },
    status: {
      relation: Model.HasOneRelation,
      modelClass: ChoiceListConnectorModel,
      join: {
        from: "work_tasks.status_id",
        to: "choice_list.id"
      }
    }
  };

  $beforeInsert() {
    this.created_at = new Date().toISOString();
    this.updated_at = new Date().toISOString();
  }

  $beforeUpdate() {
    this.updated_at = new Date().toISOString();
  }
}
