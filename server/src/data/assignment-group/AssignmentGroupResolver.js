import AssignmentGroupConnectorModel from "./AssignmentGroupConnectorModel";
import AssignmentGroupModel from "./AssignmentGroupModel";
import UserModel from "../user/UserModel";

/**
 * AssignmentGroup resolver to be used in GraphQL rootValue
 * @param args
 * @returns {*}
 */
export const getAssignmentGroups = (args, context) => {
  return AssignmentGroupConnectorModel.query()
    .eager("[groupOwner]")
    .leftJoinRelation("groupOwner", { alias: "go" })
    .then(onQueryAssignmentGroup);
};

export const getAssignmentGroupByKey = (args, context) => {
  return AssignmentGroupConnectorModel.query()
    .eager("[groupOwner,groupMembers]")
    .leftJoinRelation("groupOwner", { alias: "go" })
    .leftJoinRelation("groupMembers", { alias: "gm" })
    .findById(args.assignmentGroup)
    .then(onQueryAssignmentGroupByKey);
};

const onQueryAssignmentGroup = groups => {
  if (groups.length > 0) {
    return groups.map(assignmentGroup => {
      return new AssignmentGroupModel(
        assignmentGroup.display_label,
        assignmentGroup.display_value,
        assignmentGroup.groupOwner !== null
          ? new UserModel(
              assignmentGroup.groupOwner.username,
              assignmentGroup.groupOwner.email
            )
          : assignmentGroup.groupOwner
      );
    });
  }
  return null;
};

const onQueryAssignmentGroupByKey = group => {
  if (!group) {
    return null;
  }

  var assignmentGroup = new AssignmentGroupModel(
    group.display_label,
    group.display_value,
    group.groupOwner !== null
      ? new UserModel(group.groupOwner.username, group.groupOwner.email)
      : group.groupOwner
  );

  const members = group.groupMembers.map(member => {
    return new UserModel(member.username, member.email);
  });

  assignmentGroup.setMembers(members);
  return assignmentGroup;
};
