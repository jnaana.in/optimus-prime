/**
 * GraphQL AssignmentGroupType
 */
export const AssignmentGroupType = `

type AssignmentGroup {
  displayLabel: String
  displayValue: String
  owner:User
}

type AssignmentGroupByKey {
  displayLabel: String
  displayValue: String
  owner:User
  members:[User]
}

`;
