import Model from "../KnexModel";
import UserConnectorModel from "../user/UserConnectorModel";

/**
 * Assignment Group Connector Model
 */
export default class AssignmentGroupConnectorModel extends Model {
  static tableName = "assignment_group";
  static idColumn = "display_value";

  // JsonSchema to validate data.
  static jsonSchema = {
    type: "object",
    required: ["display_label", "display_value"],
    properties: {
      display_label: { type: "string" },
      display_value: { type: "string" },
      owner: { type: "string" },
      created_at: { type: "date" },
      updated_at: { type: "date" }
    }
  };

  static relationMappings = {
    groupOwner: {
      relation: Model.HasOneRelation,
      modelClass: UserConnectorModel,
      join: {
        from: "assignment_group.owner",
        to: "users.username"
      }
    },
    groupMembers: {
      relation: Model.ManyToManyRelation,
      modelClass: UserConnectorModel,
      join: {
        from: "assignment_group.display_value",
        // ManyToMany relation needs the `through` object
        // to describe the join table.
        through: {
          // If you have a model class for the join table
          // you need to specify it like this:
          // modelClass: PersonMovie,
          from: "assignment_group_members.group",
          to: "assignment_group_members.member"
        },
        to: "users.username"
      }
    }
  };

  $beforeInsert() {
    this.created_at = new Date().toISOString();
    this.updated_at = new Date().toISOString();
  }

  $beforeUpdate() {
    this.updated_at = new Date().toISOString();
  }
}
