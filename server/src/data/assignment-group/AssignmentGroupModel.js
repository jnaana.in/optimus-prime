/**
 * AssignmentGroup Model
 */
export default class AssignmentGroupModel {
  constructor(displayLabel, displayValue, owner) {
    this.displayLabel = displayLabel;
    this.displayValue = displayValue;
    this.owner = owner;
    this.members = [];
  }

  addMember(member) {
    this.members.push(member);
  }

  setMembers(members) {
    this.members = members;
  }
}
