import express from "express";
import path from "path";
import favicon from "serve-favicon";
import logger from "morgan";
import cookieParser from "cookie-parser";
import bodyParser from "body-parser";
import { graphqlExpress } from "graphql-server-express";
import GraphQLHTTP from "express-graphql";
import { graphql } from "graphql";
import { config } from "./data/graphqlConfig";
import dotenv from "dotenv";
import jwt from "express-jwt";

// Uncomment after placing your favicon in /public
const PORT = 3000;
const app = express();
dotenv.load();

app.use(logger("dev"));
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: false }));
app.use(cookieParser());
app.use(express.static("public"));
app.use(favicon(path.join(__dirname, "../public/assets/", "favicon.ico")));
app.set("view engine", "pug");
app.get("/", function(req, res) {
  res.render("index");
});

// bodyParser is needed just for POST.
app.use("/api/graph", GraphQLHTTP(config));

app.listen(PORT);
