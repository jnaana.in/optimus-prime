#!/bin/bash

# Delete and recreate the database
psql -U postgres -h 127.0.0.1 -c 'DROP DATABASE IF EXISTS optimusprime;'
psql -U postgres -h 127.0.0.1 -c 'CREATE DATABASE optimusprime;'
psql -U postgres -h 127.0.0.1 -c 'GRANT CONNECT ON DATABASE optimusprime TO root;'

# Run migrations to generate the schema
../node_modules/.bin/knex migrate:latest DEBUG=knex:tx

../node_modules/.bin/knex seed:run