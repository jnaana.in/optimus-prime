exports.seed = function(knex, Promise) {
  // Deletes ALL existing entries
  return knex("assignment_group").del().then(function() {
    // Inserts seed entries
    return knex("assignment_group").insert([
      {
        display_label: "Software",
        display_value: "software",
        owner: "senthilpk"
      },
      {
        display_label: "Hardware",
        display_value: "hardware",
        owner: "senthilpk"
      },
      {
        display_label: "Database Oracle",
        display_value: "database_oracle",
        owner: "senthilpk"
      },
      {
        display_label: "Database Postgres",
        display_value: "database_postgres",
        owner: "senthilpk"
      },
      {
        display_label: "Webserver",
        display_value: "webserver",
        owner: "senthilpk"
      },
      {
        display_label: "Unix Technical Services",
        display_value: "uts",
        owner: "senthilpk"
      },
      {
        display_label: "Windows Technical Services",
        display_value: "wts",
        owner: "senthilpk"
      }
    ]);
  });
};
