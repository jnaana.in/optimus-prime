exports.seed = function(knex, Promise) {
  // Deletes ALL existing entries
  return knex("assignment_group_members").del().then(function() {
    // Inserts seed entries
    return knex("assignment_group_members").insert([
      { group: "software", member: "senthilpk" },
      { group: "hardware", member: "admin" }
    ]);
  });
};
