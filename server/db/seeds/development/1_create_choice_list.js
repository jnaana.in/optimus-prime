exports.seed = function(knex, Promise) {
  // Deletes ALL existing entries
  return knex("choice_list").del().then(function() {
    // Inserts seed entries
    return knex("choice_list").insert([
      {
        table: "work_task",
        field: "request_type",
        display_label: "Request",
        display_value: "request",
        sort_order: 1
      },
      {
        table: "work_task",
        field: "request_type",
        display_label: "Incident",
        display_value: "incident",
        sort_order: 2
      },
      {
        table: "work_task",
        field: "request_type",
        display_label: "Question",
        display_value: "question",
        sort_order: 3
      },
      {
        table: "work_task",
        field: "status",
        display_label: "New",
        display_value: "new",
        sort_order: 1
      },
      {
        table: "work_task",
        field: "status",
        display_label: "Investigation",
        display_value: "investigation",
        sort_order: 2
      },
      {
        table: "work_task",
        field: "status",
        display_label: "Confirmed",
        display_value: "confirmed",
        sort_order: 3
      },
      {
        table: "work_task",
        field: "status",
        display_label: "Work In Progress",
        display_value: "work_in_progress",
        sort_order: 4
      },
      {
        table: "work_task",
        field: "status",
        display_label: "Resolved",
        display_value: "resolved",
        sort_order: 5
      },
      {
        table: "work_task",
        field: "status",
        display_label: "No Fix",
        display_value: "no_fix",
        sort_order: 6
      },
      {
        table: "work_task",
        field: "status",
        display_label: "Closed",
        display_value: "closed",
        sort_order: 7
      }
    ]);
  });
};
