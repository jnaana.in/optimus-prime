exports.seed = function(knex, Promise) {
  // Deletes ALL existing entries
  return knex("users").del().then(function() {
    // Inserts seed entries
    return knex("users").insert([
      {
        username: "senthilpk",
        email: "senthilpk@ymail.com",
        password: "$2a$08$LV3nqit.fHL22PBaQMmn/OmcQlO64Gd5S3wXKrOe5m/ubpmy7Hp7."
      },
      {
        username: "admin",
        email: "admin@gmail.com",
        password: "$2a$08$LV3nqit.fHL22PBaQMmn/OmcQlO64Gd5S3wXKrOe5m/ubpmy7Hp7."
      }
    ]);
  });
};
