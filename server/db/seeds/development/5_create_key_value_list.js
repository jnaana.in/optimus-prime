exports.seed = function(knex, Promise) {
  // Deletes ALL existing entries
  return knex("key_value_list").del().then(function() {
    // Inserts seed entries
    return knex("key_value_list").insert([
      {
        key: "sortable_field_tasks",
        display_label: "Assignee",
        display_value: "assignee",
        sort_order: 1
      },
      {
        key: "sortable_field_tasks",
        display_label: "Caller",
        display_value: "caller",
        sort_order: 2
      },
      {
        key: "sortable_field_tasks",
        display_label: "Task Type",
        display_value: "task_type_id",
        sort_order: 3
      },
      {
        key: "sortable_field_tasks",
        display_label: "Status ",
        display_value: "status_id",
        sort_order: 4
      },
      {
        key: "sortable_field_tasks",
        display_label: "Created At",
        display_value: "created_at",
        sort_order: 5
      },
      {
        key: "sortable_field_tasks",
        display_label: "Updated At",
        display_value: "update_at",
        sort_order: 6
      }
    ]);
  });
};
