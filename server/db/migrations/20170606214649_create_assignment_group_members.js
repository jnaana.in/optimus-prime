exports.up = function(knex, Promise) {
  Promise.all([
    knex.schema.createTable("assignment_group_members", function(table) {
      table.string("member").references("username").inTable("users");
      table
        .string("group")
        .references("display_value")
        .inTable("assignment_group");
      table.timestamps();
    })
  ]);
};

exports.down = function(knex, Promise) {
  Promise.all([knex.schema.dropTable("assignment_group_members")]);
};
