exports.up = function(knex, Promise) {
  return Promise.all([
    knex.schema
      .raw("CREATE SEQUENCE work_tasks_task_id_seq START 100000;")
      .createTable("work_tasks", function(table) {
        table.string("title", 150).notNullable();
        table.text("description");
        table.string("caller").references("username").inTable("users");
        table.string("assignee").references("username").inTable("users");
        table
          .string("group")
          .references("display_value")
          .inTable("assignment_group");
        table.integer("task_type_id").references("id").inTable("choice_list");
        table
          .integer("status_id")
          .unsigned()
          .references("id")
          .inTable("choice_list");
        table.timestamps();
      })
      .raw(
        "alter table work_tasks add column task_id  text  NOT NULL DEFAULT ('TSK' || nextval('work_tasks_task_id_seq'))"
      )
      .raw("ALTER SEQUENCE work_tasks_task_id_seq OWNED BY work_tasks.task_id;")
  ]);
};

exports.down = function(knex, Promise) {
  return Promise.all([knex.schema.dropTable("work_tasks")]);
};
