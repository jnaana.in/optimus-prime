exports.up = function(knex, Promise) {
  return Promise.all([
    knex.schema.createTable("choice_list", function(table) {
      table.increments();
      table.string("table", 254).notNullable();
      table.string("field", 254).notNullable();
      table.string("display_label", 254).notNullable();
      table.string("display_value", 60).notNullable();
      table.integer("sort_order").notNullable();
      table.timestamps();
    })
  ]);
};

exports.down = function(knex, Promise) {
  return Promise.all([knex.schema.dropTable("choice_list")]);
};
