exports.up = function(knex, Promise) {
  Promise.all([
    knex.schema.createTable("assignment_group", function(table) {
      table.string("display_label", 254).notNullable();
      table.string("display_value", 60).unique().notNullable();
      table.string("owner").references("username").inTable("users");
      table.timestamps();
    })
  ]);
};

exports.down = function(knex, Promise) {
  Promise.all([knex.schema.dropTable("assignment_group")]);
};
