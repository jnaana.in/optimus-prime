exports.up = function(knex, Promise) {
  return Promise.all([
    knex.schema.createTable("key_value_list", function(table) {
      table.increments();
      table.string("key", 254).notNullable();
      table.string("display_label", 254).notNullable();
      table.string("display_value", 60).notNullable();
      table.integer("sort_order");
      table.timestamps();
    })
  ]);
};

exports.down = function(knex, Promise) {};
