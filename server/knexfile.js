// Update with your config settings.

module.exports = {

  development: {
    client: 'postgresql',
    connection: {
      database: 'optimusprime',
      user:     'root',
      password: ''
    },
    migrations: {
      tableName: 'knex_migrations',
      directory: './db/migrations'
    },
    seeds:{
      directory: './db/seeds/development'
    }
  },

  staging: {
    client: 'postgresql',
    connection: {
      database: 'optimusprime',
      user:     'root',
      password: ''
    },
    pool: {
      min: 2,
      max: 10
    },
    migrations: {
      tableName: 'knex_migrations',
      directory: './db/migrations'
    },
    seeds:{
      directory: './db/seeds/staging'
    }
  },

  production: {
    client: 'postgresql',
    connection:{
      database: 'optimusprime',
      user:     'root',
      password: ''
    },
    pool: {
      min: 2,
      max: 10
    },
    migrations: {
      tableName: 'knex_migrations',
      directory: './db/migrations'
    },
    seeds:{
      directory: './db/seeds/development'
    },
    debug:true
  }

};
